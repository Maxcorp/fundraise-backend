import 'babel-polyfill';
import Router from 'koa-router';
import { baseApi } from '../config';
import jwt from '../middlewares/jwt';
import DonateControllers from '../controllers/donate';

const router = new Router();

// const api = 'donates';

// GET /all
// router.prefix(`/${baseApi}/${api}`);

router.get('/', DonateControllers.find);

// POST /donate
router.post('/donate', DonateControllers.donate);

// router.post('/', DonateControllers.add);


export default router;
