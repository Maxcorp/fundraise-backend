import Donate from '../models/donate';
import City from "../models/cities";

class DonateControllers {
  /* eslint-disable no-param-reassign */

  /**
   * Get all donates
   * @param {ctx} Koa Context
   */
  async find(ctx) {
    ctx.body = await Donate.find();
  }

  /**
   * Add a donate
   * @param {ctx} Koa Context
   */
  async donate(ctx) {
    const currencies = ['USD', 'EUR', 'GBP', 'RUB'];
    // console.log(ctx.request.body.amount);

    if(ctx.request.body.amount > 0 && currencies.includes(ctx.request.body.currency)) {
      try {
        const donate = await new Donate(ctx.request.body).save();
        ctx.body = { ok: true };
      } catch (err) {
        console.log(err);
        ctx.body = { ok: false };
      }
    } else {
      ctx.body = { ok: false };
    }
  }

  /* eslint-enable no-param-reassign */
}

export default new DonateControllers();
